package com.abc.accountservice.service;

import com.abc.accountservice.model.Account;
import com.abc.accountservice.model.AccountProduct;
import com.abc.accountservice.model.AccountResponse;
import com.abc.accountservice.repository.AccountDAO;
import com.abc.accountservice.repository.AccountProductDAO;
import com.abc.accountservice.repository.AccountProductRepo;
import com.abc.accountservice.repository.AccountRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    private AccountRepo accountRepo;
    @Mock
    private AccountProductRepo accountProductRepo;
    @InjectMocks
    private AccountService accountService;


    @Test
    public void getAllAccount() {

        AccountDAO accountDAO = new AccountDAO();
        AccountProductDAO accountProductDAO = new AccountProductDAO();
        List<AccountDAO> accountDAOList = new ArrayList<>();

        accountDAO.setAccountNo("12345");
        accountDAO.setIsHidden(Boolean.FALSE);
        accountDAO.setProductId(1);
        accountDAOList.add(accountDAO);

        accountProductDAO.setProductId(1);
        accountProductDAO.setProductCode("001C");
        accountProductDAO.setProductName("Basic Current Account");
        accountProductDAO.setProductType("Current");

        //Expected
        AccountResponse accountResponse = new AccountResponse();
        AccountProduct accountProduct = new AccountProduct();
        Account account = new Account();

        account.setAccountProduct(accountProduct);
        accountResponse.setAccounts(new ArrayList<>());

        account.setAccountNo("12345");
        account.setIsHidden(Boolean.FALSE);
        account.getAccountProduct().setProductId(1);
        account.getAccountProduct().setProductCode("001C");
        account.getAccountProduct().setProductName("Basic Current Account");
        account.getAccountProduct().setProductType("Current");
        accountResponse.getAccounts().add(account);

        Mockito.when(accountRepo.findAll()).thenReturn(accountDAOList);
        Mockito.when(accountProductRepo.findProductById(1)).thenReturn(accountProductDAO);

        AccountResponse actual = accountService.getAllAccount();

        assertEquals(accountResponse, actual);

    }
}