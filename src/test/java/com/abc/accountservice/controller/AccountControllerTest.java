package com.abc.accountservice.controller;

import com.abc.accountservice.model.AccountResponse;
import com.abc.accountservice.service.AccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountControllerTest {

    @InjectMocks
    private AccountController accountController;
    @Mock
    private AccountService accountService;
    @Mock
    private HttpHeaders httpHeaders;

    @Test
    public void accountInquiry() {

        AccountResponse accountResponse = new AccountResponse();
        Mockito.when(accountService.getAllAccount()).thenReturn(accountResponse);

        ResponseEntity actual = accountController.accountInquiry();

        assertEquals(accountResponse,actual.getBody());
        assertEquals(HttpStatus.OK,actual.getStatusCode());
    }

    @Test
    public void accountInquiryFail(){

        Mockito.when(accountService.getAllAccount()).thenThrow(new RuntimeException());

        ResponseEntity actual = accountController.accountInquiry();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, actual.getStatusCode());

    }
}