package com.abc.accountservice.service;


import com.abc.accountservice.model.Account;
import com.abc.accountservice.model.AccountProduct;
import com.abc.accountservice.model.AccountResponse;
import com.abc.accountservice.repository.AccountDAO;
import com.abc.accountservice.repository.AccountProductDAO;
import com.abc.accountservice.repository.AccountProductRepo;
import com.abc.accountservice.repository.AccountRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountService {
    private AccountRepo accountRepo;
    private AccountProductRepo accountProductRepo;

    public AccountService(AccountRepo accountRepo, AccountProductRepo accountProductRepo) {
        this.accountRepo = accountRepo;
        this.accountProductRepo = accountProductRepo;
    }

    public AccountResponse getAllAccount() {
        AccountResponse accountResponse = new AccountResponse();
        accountResponse.setAccounts(new ArrayList<>());


        List<AccountDAO> accountDAOList = accountRepo.findAll();
        accountDAOList.forEach(accountDAO -> {
            Account account = new Account();
            AccountProduct accountProduct = new AccountProduct();
            account.setAccountProduct(accountProduct);


            AccountProductDAO accountProductDAO = accountProductRepo.findProductById(accountDAO.getProductId());
            accountResponse.getAccounts().add(accountMapper(account, accountDAO, accountProductDAO));

        });

        return accountResponse;
    }


    private Account accountMapper(Account account, AccountDAO accountDAO, AccountProductDAO accountProductDAO) {
        account.setAccountNo(accountDAO.getAccountNo());
        account.setIsHidden(accountDAO.getIsHidden());
        account.setCreatedBy(accountDAO.getCreatedBy());
        account.setCreatedTime(accountDAO.getCreatedTime());
        account.setUpdatedBy(accountDAO.getUpdatedBy());
        account.setUpdatedTime(accountDAO.getUpdatedTime());

        account.getAccountProduct().setProductId(accountProductDAO.getProductId());
        account.getAccountProduct().setProductCode(accountProductDAO.getProductCode());
        account.getAccountProduct().setProductName(accountProductDAO.getProductName());
        account.getAccountProduct().setProductType(accountProductDAO.getProductType());
        account.getAccountProduct().setCreatedBy(accountProductDAO.getCreatedBy());
        account.getAccountProduct().setCreatedTime(accountProductDAO.getCreatedTime());
        account.getAccountProduct().setUpdatedBy(accountProductDAO.getUpdatedBy());
        account.getAccountProduct().setUpdatedTime(accountProductDAO.getUpdatedTime());

        return account;
    }


}
