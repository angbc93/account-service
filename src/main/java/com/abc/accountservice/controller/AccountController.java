package com.abc.accountservice.controller;


import com.abc.accountservice.model.AccountResponse;
import com.abc.accountservice.service.AccountService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

    private AccountService accountService;
    private HttpHeaders httpHeaders;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping(path = "/account-service")
    public ResponseEntity accountInquiry() {

        try{
            AccountResponse accountResponse = accountService.getAllAccount();
            return new ResponseEntity<>(accountResponse, httpHeaders, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(e,httpHeaders,HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }


}


