package com.abc.accountservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AccountProductRepo extends JpaRepository<AccountProductDAO, Integer> {

    @Query(value = "SELECT * FROM tbl_account_product WHERE product_id = :id",
        nativeQuery = true)
    AccountProductDAO findProductById(@Param("id") Integer id);


}
