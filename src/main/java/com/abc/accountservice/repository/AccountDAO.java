package com.abc.accountservice.repository;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "tbl_account")
public class AccountDAO {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "account_no")
    private String accountNo;

    @Column(name = "is_hidden")
    private Boolean isHidden;

    @Column(name = "product_id")
    private int productId;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTime;

}
